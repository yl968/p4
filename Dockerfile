# Use Rust official image as the builder stage
FROM rust:latest as builder
WORKDIR /usr/src/myapp
COPY . .
RUN cargo install --path .

# Use Debian slim for the runtime stage for a smaller image
FROM rust:latest
COPY --from=builder /usr/local/cargo/bin/myapp /usr/local/bin/myapp

# Expose port 8080 and run the binary
EXPOSE 8080
CMD ["myapp"]
