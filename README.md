
# Rust Actix Web Application

This README outlines the steps to create a simple Rust Actix web application and containerize it using Docker for easy deployment and scaling.


## Creating the Actix Web Application

### Step 1: Set Up a New Rust Project

Create a new binary project using Cargo and navigate into the project directory:

```bash
cargo new actix-web-app --bin
cd actix-web-app
```

### Step 2: Add Actix Web Dependency

Edit the `Cargo.toml` file to include Actix Web as a dependency:

```toml
[dependencies]
actix-web = "4.0"
```

### Step 3: Implement the Web Server

Replace the content of `src/main.rs` with the following code to create a basic HTTP server:

```rust
use actix_web::{web, App, HttpResponse, HttpServer, Responder};

async fn greet() -> impl Responder {
    HttpResponse::Ok().body("Hello, Mini Proj4!")
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/", web::get().to(greet))
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
```


## Containerizing with Docker

### Step 1: Create a Dockerfile

In the root of the Rust project, create a `Dockerfile` with the following content:

```Dockerfile
FROM rust:latest as builder
WORKDIR /usr/src/myapp
COPY . .
RUN cargo install --path .

FROM rust:latest
COPY --from=builder /usr/local/cargo/bin/myapp /usr/local/bin/myapp

EXPOSE 8080
CMD ["myapp"]
```

### Step 2: Build the Docker Image

Build the Docker image from the Dockerfile:

```bash
docker build -t myapp .
```

### Step 3: Run the Container

Start a Docker container from the image, mapping port 8080:

```bash
docker run -d -p 8080:8080 myappp
```

The Actix web application is now running inside a Docker container and is accessible at `http://localhost:8080`.

### Docker Container Running

Below is a screenshot showing the Docker container running in Docker Desktop:

![Docker Container Running](./a0d583afebaf511a2901acb547f9449.png)
![Docker Container Running](./67529b05fa0365373a68fb943cfc979.png)